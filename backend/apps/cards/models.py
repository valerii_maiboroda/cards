from django.core.validators import MaxLengthValidator
from django.db import models


# Create your models here.
from apps.users.models import User


class Card(models.Model):
    original_name = models.CharField(max_length=30)
    translation = models.CharField(max_length=30)
    level = models.ForeignKey(to='cards.Level', on_delete=models.PROTECT)
    user = models.ForeignKey(to='users.User', on_delete=models.PROTECT)

    def __str__(self):
        return self.original_name + self.translation


class Level(models.Model):
    number_level = models.IntegerField()
    description = models.TextField(validators=[MaxLengthValidator(2000)])
    user = models.OneToOneField(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.number_level
