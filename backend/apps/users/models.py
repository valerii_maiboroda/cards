from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

# Create your models here.


class User(AbstractBaseUser):
    username = None
    email = models.CharField(max_length=30)

    def __str__(self):
        return self.email
